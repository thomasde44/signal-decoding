INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_WIFI_PLUTO wifi_pluto)

FIND_PATH(
    WIFI_PLUTO_INCLUDE_DIRS
    NAMES wifi_pluto/api.h
    HINTS $ENV{WIFI_PLUTO_DIR}/include
        ${PC_WIFI_PLUTO_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    WIFI_PLUTO_LIBRARIES
    NAMES gnuradio-wifi_pluto
    HINTS $ENV{WIFI_PLUTO_DIR}/lib
        ${PC_WIFI_PLUTO_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/wifi_plutoTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(WIFI_PLUTO DEFAULT_MSG WIFI_PLUTO_LIBRARIES WIFI_PLUTO_INCLUDE_DIRS)
MARK_AS_ADVANCED(WIFI_PLUTO_LIBRARIES WIFI_PLUTO_INCLUDE_DIRS)
