/* -*- c++ -*- */

#define WIFI_PLUTO_API
#define DIGITAL_API
%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "wifi_pluto_swig_doc.i"

%{
#include "wifi_pluto/short_frame_sync.h"
#include "wifi_pluto/long_frame_synch.h"
#include "wifi_pluto/constellations.h"
#include "wifi_pluto/frame_equalizer.h"
#include "wifi_pluto/mapper.h"
#include "wifi_pluto/decode_mac.h"
#include "wifi_pluto/mac.h"
#include "wifi_pluto/signal_field.h"
#include "wifi_pluto/parse_mac.h"
%}

%ignore gr::digital::constellation_bpsk;
%ignore gr::digital::constellation_qpsk;
%ignore gr::digital::constellation_16qam;
%include "gnuradio/digital/constellation.h"

%include "wifi_pluto/short_frame_sync.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, short_frame_sync);
%include "wifi_pluto/long_frame_synch.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, long_frame_synch);
%include "wifi_pluto/constellations.h"
%include "wifi_pluto/frame_equalizer.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, frame_equalizer);
%include "wifi_pluto/mapper.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, mapper);
%include "wifi_pluto/decode_mac.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, decode_mac);



%template(constellation_bpsk_sptr) boost::shared_ptr<gr::wifi_pluto::constellation_bpsk>;
%pythoncode %{
constellation_bpsk_sptr.__repr__ = lambda self: "<constellation BPSK>"
constellation_bpsk = constellation_bpsk.make;
%}

%template(constellation_qpsk_sptr) boost::shared_ptr<gr::wifi_pluto::constellation_qpsk>;
%pythoncode %{
constellation_qpsk_sptr.__repr__ = lambda self: "<constellation QPSK>"
constellation_qpsk = constellation_qpsk.make;
%}

%template(constellation_16qam_sptr) boost::shared_ptr<gr::wifi_pluto::constellation_16qam>;
%pythoncode %{
constellation_16qam_sptr.__repr__ = lambda self: "<constellation 16QAM>"
constellation_16qam = constellation_16qam.make;
%}

%template(constellation_64qam_sptr) boost::shared_ptr<gr::wifi_pluto::constellation_64qam>;
%pythoncode %{
constellation_64qam_sptr.__repr__ = lambda self: "<constellation 64QAM>"
constellation_64qam = constellation_64qam.make;
%}

%include "wifi_pluto/mac.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, mac);
%include "wifi_pluto/signal_field.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, signal_field);
%include "wifi_pluto/parse_mac.h"
GR_SWIG_BLOCK_MAGIC2(wifi_pluto, parse_mac);
