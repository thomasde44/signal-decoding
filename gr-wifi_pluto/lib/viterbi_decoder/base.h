#ifndef INCLUDED_WIFI_PLUTO_VITERBI_DECODER_BASE_H
#define INCLUDED_WIFI_PLUTO_VITERBI_DECODER_BASE_H

#include "../utils.h"

namespace gr {
namespace wifi_pluto {

// Maximum number of traceback bytes
#define TRACEBACK_MAX 24

/* This Viterbi decoder was taken from the gr-dvbt module of
 * GNU Radio. It is an SSE2 version of the Viterbi Decoder
 * created by Phil Karn. The SSE2 version was made by Bogdan
 * Diaconescu. For more info see: gr-dvbt/lib/d_viterbi.h
 */
class base
{
public:
    base();
    ~base();
    virtual uint8_t* decode(ofdm_param* ofdm, frame_param* frame, uint8_t* in) = 0;

protected:
    // Position in circular buffer where the current decoded byte is stored
    int d_store_pos;
    // Metrics for each state
    unsigned char d_mmresult[64] __attribute__((aligned(16)));
    // Paths for each state
    unsigned char d_ppresult[TRACEBACK_MAX][64] __attribute__((aligned(16)));

    int d_ntraceback;
    int d_k;
    ofdm_param* d_ofdm;
    frame_param* d_frame;
    const unsigned char* d_depuncture_pattern;

    uint8_t d_depunctured[MAX_ENCODED_BITS];
    uint8_t d_decoded[MAX_ENCODED_BITS * 3 / 4];

    static const unsigned char PARTAB[256];
    static const unsigned char PUNCTURE_1_2[2];
    static const unsigned char PUNCTURE_2_3[4];
    static const unsigned char PUNCTURE_3_4[6];

    virtual void reset() = 0;
    uint8_t* depuncture(uint8_t* in);
};

} // namespace wifi_pluto
} // namespace gr

#endif /* INCLUDED_WIFI_PLUTO_VITERBI_DECODER_BASE_H */
