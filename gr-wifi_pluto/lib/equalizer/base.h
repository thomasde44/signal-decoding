#ifndef INCLUDED_WIFI_PLUTO_EQUALIZER_BASE_H
#define INCLUDED_WIFI_PLUTO_EQUALIZER_BASE_H

#include <gnuradio/digital/constellation.h>
#include <gnuradio/gr_complex.h>

namespace gr {
namespace wifi_pluto {
namespace equalizer {

class base
{
public:
    virtual ~base(){};
    virtual void equalize(gr_complex* in,
                          int n,
                          gr_complex* symbols,
                          uint8_t* bits,
                          boost::shared_ptr<gr::digital::constellation> mod) = 0;
    virtual double get_snr() = 0;

    static const gr_complex POLARITY[127];

    std::vector<gr_complex> get_csi();

protected:
    static const gr_complex LONG[64];

    gr_complex d_H[64];
};

} // namespace equalizer
} /* namespace wifi_pluto */
} /* namespace gr */

#endif /* INCLUDED_WIFI_PLUTO_EQUALIZER_BASE_H */
