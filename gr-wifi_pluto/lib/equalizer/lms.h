#ifndef INCLUDED_WIFI_PLUTO_EQUALIZER_LMS_H
#define INCLUDED_WIFI_PLUTO_EQUALIZER_LMS_H

#include "base.h"
#include <vector>

namespace gr {
namespace wifi_pluto {
namespace equalizer {

class lms : public base
{
public:
    virtual void equalize(gr_complex* in,
                          int n,
                          gr_complex* symbols,
                          uint8_t* bits,
                          boost::shared_ptr<gr::digital::constellation> mod);

private:
    double get_snr();

    double d_snr;
    const double alpha = 0.5;
};

} // namespace equalizer
} /* namespace wifi_pluto */
} /* namespace gr */

#endif /* INCLUDED_WIFI_PLUTO_EQUALIZER_LMS_H */
