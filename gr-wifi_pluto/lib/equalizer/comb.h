#ifndef INCLUDED_WIFI_PLUTO_EQUALIZER_COMB_H
#define INCLUDED_WIFI_PLUTO_EQUALIZER_COMB_H

#include "base.h"

namespace gr {
namespace wifi_pluto {
namespace equalizer {

class comb : public base
{
public:
    virtual void equalize(gr_complex* in,
                          int n,
                          gr_complex* symbols,
                          uint8_t* bits,
                          boost::shared_ptr<gr::digital::constellation> mod);
    double get_snr();

private:
    const double alpha = 0.2;
};

} // namespace equalizer
} /* namespace wifi_pluto */
} /* namespace gr */

#endif /* INCLUDED_WIFI_PLUTO_EQUALIZER_COMB_H */
