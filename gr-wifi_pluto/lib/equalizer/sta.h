#ifndef INCLUDED_WIFI_PLUTO_EQUALIZER_STA_H
#define INCLUDED_WIFI_PLUTO_EQUALIZER_STA_H

#include "base.h"
#include <vector>

namespace gr {
namespace wifi_pluto {
namespace equalizer {

class sta : public base
{
public:
    virtual void equalize(gr_complex* in,
                          int n,
                          gr_complex* symbols,
                          uint8_t* bits,
                          boost::shared_ptr<gr::digital::constellation> mod);
    double get_snr();

private:
    double d_snr;

    const double alpha = 0.5;
    const int beta = 2;
};

} // namespace equalizer
} /* namespace wifi_pluto */
} /* namespace gr */

#endif /* INCLUDED_WIFI_PLUTO_EQUALIZER_STA_H */
