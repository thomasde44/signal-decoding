#ifndef INCLUDED_WIFI_PLUTO_EQUALIZER_LS_H
#define INCLUDED_WIFI_PLUTO_EQUALIZER_LS_H

#include "base.h"
#include <vector>

namespace gr {
namespace wifi_pluto {
namespace equalizer {

class ls : public base
{
public:
    virtual void equalize(gr_complex* in,
                          int n,
                          gr_complex* symbols,
                          uint8_t* bits,
                          boost::shared_ptr<gr::digital::constellation> mod);
    virtual double get_snr();

private:
    double d_snr;
};

} // namespace equalizer
} /* namespace wifi_pluto */
} /* namespace gr */

#endif /* INCLUDED_WIFI_PLUTO_EQUALIZER_LS_H */
