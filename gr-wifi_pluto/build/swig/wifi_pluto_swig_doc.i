
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::wifi_pluto::constellation_16qam "

Constructor Specific Documentation:

"



%feature("docstring") gr::wifi_pluto::constellation_16qam::make "

Constructor Specific Documentation:

"

%feature("docstring") gr::wifi_pluto::constellation_64qam "

Constructor Specific Documentation:

"



%feature("docstring") gr::wifi_pluto::constellation_64qam::make "

Constructor Specific Documentation:

"

%feature("docstring") gr::wifi_pluto::constellation_bpsk "<+description of block+>

Constructor Specific Documentation:

"



%feature("docstring") gr::wifi_pluto::constellation_bpsk::make "<+description of block+>

Constructor Specific Documentation:

"

%feature("docstring") gr::wifi_pluto::constellation_qpsk "

Constructor Specific Documentation:

"



%feature("docstring") gr::wifi_pluto::constellation_qpsk::make "

Constructor Specific Documentation:

"

%feature("docstring") gr::wifi_pluto::decode_mac "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::decode_mac.

To avoid accidental use of raw pointers, wifi_pluto::decode_mac's constructor is in a private implementation class. wifi_pluto::decode_mac::make is the public interface for creating new instances.

Args:
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::decode_mac::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::decode_mac.

To avoid accidental use of raw pointers, wifi_pluto::decode_mac's constructor is in a private implementation class. wifi_pluto::decode_mac::make is the public interface for creating new instances.

Args:
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::frame_equalizer "<+description of block+>

Constructor Specific Documentation:



Args:
    algo : 
    freq : 
    bw : 
    log : 
    debug : "







%feature("docstring") gr::wifi_pluto::frame_equalizer::make "<+description of block+>

Constructor Specific Documentation:



Args:
    algo : 
    freq : 
    bw : 
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::long_frame_synch "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::long_frame_synch.

To avoid accidental use of raw pointers, wifi_pluto::long_frame_synch's constructor is in a private implementation class. wifi_pluto::long_frame_synch::make is the public interface for creating new instances.

Args:
    sync_length : 
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::long_frame_synch::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::long_frame_synch.

To avoid accidental use of raw pointers, wifi_pluto::long_frame_synch's constructor is in a private implementation class. wifi_pluto::long_frame_synch::make is the public interface for creating new instances.

Args:
    sync_length : 
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::mac "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::mac.

To avoid accidental use of raw pointers, wifi_pluto::mac's constructor is in a private implementation class. wifi_pluto::mac::make is the public interface for creating new instances.

Args:
    src_mac : 
    dst_mac : 
    bss_mac : "

%feature("docstring") gr::wifi_pluto::mac::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::mac.

To avoid accidental use of raw pointers, wifi_pluto::mac's constructor is in a private implementation class. wifi_pluto::mac::make is the public interface for creating new instances.

Args:
    src_mac : 
    dst_mac : 
    bss_mac : "

%feature("docstring") gr::wifi_pluto::mapper "<+description of block+>

Constructor Specific Documentation:



Args:
    mcs : 
    debug : "



%feature("docstring") gr::wifi_pluto::mapper::make "<+description of block+>

Constructor Specific Documentation:



Args:
    mcs : 
    debug : "

%feature("docstring") gr::wifi_pluto::parse_mac "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::parse_mac.

To avoid accidental use of raw pointers, wifi_pluto::parse_mac's constructor is in a private implementation class. wifi_pluto::parse_mac::make is the public interface for creating new instances.

Args:
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::parse_mac::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::parse_mac.

To avoid accidental use of raw pointers, wifi_pluto::parse_mac's constructor is in a private implementation class. wifi_pluto::parse_mac::make is the public interface for creating new instances.

Args:
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::short_frame_sync "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::short_frame_sync.

To avoid accidental use of raw pointers, wifi_pluto::short_frame_sync's constructor is in a private implementation class. wifi_pluto::short_frame_sync::make is the public interface for creating new instances.

Args:
    threshold : 
    min_plateau : 
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::short_frame_sync::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::short_frame_sync.

To avoid accidental use of raw pointers, wifi_pluto::short_frame_sync's constructor is in a private implementation class. wifi_pluto::short_frame_sync::make is the public interface for creating new instances.

Args:
    threshold : 
    min_plateau : 
    log : 
    debug : "

%feature("docstring") gr::wifi_pluto::signal_field "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::signal_field.

To avoid accidental use of raw pointers, wifi_pluto::signal_field's constructor is in a private implementation class. wifi_pluto::signal_field::make is the public interface for creating new instances."



%feature("docstring") gr::wifi_pluto::signal_field::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of wifi_pluto::signal_field.

To avoid accidental use of raw pointers, wifi_pluto::signal_field's constructor is in a private implementation class. wifi_pluto::signal_field::make is the public interface for creating new instances."