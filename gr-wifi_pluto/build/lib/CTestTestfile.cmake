# CMake generated Testfile for 
# Source directory: /home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib
# Build directory: /home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(wifi_pluto_qa_decode_mac.cc "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/lib/wifi_pluto_qa_decode_mac.cc_test.sh")
set_tests_properties(wifi_pluto_qa_decode_mac.cc PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/usr/local/lib/cmake/gnuradio/GrTest.cmake;172;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;128;GR_ADD_CPP_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;0;")
add_test(wifi_pluto_qa_frame_equalizer.cc "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/lib/wifi_pluto_qa_frame_equalizer.cc_test.sh")
set_tests_properties(wifi_pluto_qa_frame_equalizer.cc PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/usr/local/lib/cmake/gnuradio/GrTest.cmake;172;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;128;GR_ADD_CPP_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;0;")
add_test(wifi_pluto_qa_long_frame_synch.cc "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/lib/wifi_pluto_qa_long_frame_synch.cc_test.sh")
set_tests_properties(wifi_pluto_qa_long_frame_synch.cc PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/usr/local/lib/cmake/gnuradio/GrTest.cmake;172;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;128;GR_ADD_CPP_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;0;")
add_test(wifi_pluto_qa_short_frame_sync.cc "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/lib/wifi_pluto_qa_short_frame_sync.cc_test.sh")
set_tests_properties(wifi_pluto_qa_short_frame_sync.cc PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/usr/local/lib/cmake/gnuradio/GrTest.cmake;172;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;128;GR_ADD_CPP_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/lib/CMakeLists.txt;0;")
