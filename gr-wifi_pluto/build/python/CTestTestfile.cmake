# CMake generated Testfile for 
# Source directory: /home/meanthatmuch/signal-decoding/gr-wifi_pluto/python
# Build directory: /home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(qa_short_frame_sync "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/python/qa_short_frame_sync_test.sh")
set_tests_properties(qa_short_frame_sync PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;45;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;0;")
add_test(qa_long_frame_synch "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/python/qa_long_frame_synch_test.sh")
set_tests_properties(qa_long_frame_synch PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;46;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;0;")
add_test(qa_frame_equalizer "/usr/bin/sh" "/home/meanthatmuch/signal-decoding/gr-wifi_pluto/build/python/qa_frame_equalizer_test.sh")
set_tests_properties(qa_frame_equalizer PROPERTIES  _BACKTRACE_TRIPLES "/usr/local/lib/cmake/gnuradio/GrTest.cmake;122;add_test;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;47;GR_ADD_TEST;/home/meanthatmuch/signal-decoding/gr-wifi_pluto/python/CMakeLists.txt;0;")
