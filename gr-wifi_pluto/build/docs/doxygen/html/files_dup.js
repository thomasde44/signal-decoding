var files_dup =
[
    [ "api.h", "api_8h.html", "api_8h" ],
    [ "equalizer/base.h", "equalizer_2base_8h.html", [
      [ "base", "classgr_1_1wifi__pluto_1_1equalizer_1_1base.html", "classgr_1_1wifi__pluto_1_1equalizer_1_1base" ]
    ] ],
    [ "viterbi_decoder/base.h", "viterbi__decoder_2base_8h.html", "viterbi__decoder_2base_8h" ],
    [ "comb.h", "comb_8h.html", [
      [ "comb", "classgr_1_1wifi__pluto_1_1equalizer_1_1comb.html", "classgr_1_1wifi__pluto_1_1equalizer_1_1comb" ]
    ] ],
    [ "constellations.h", "constellations_8h.html", [
      [ "constellation_bpsk", "classgr_1_1wifi__pluto_1_1constellation__bpsk.html", "classgr_1_1wifi__pluto_1_1constellation__bpsk" ],
      [ "constellation_qpsk", "classgr_1_1wifi__pluto_1_1constellation__qpsk.html", "classgr_1_1wifi__pluto_1_1constellation__qpsk" ],
      [ "constellation_16qam", "classgr_1_1wifi__pluto_1_1constellation__16qam.html", "classgr_1_1wifi__pluto_1_1constellation__16qam" ],
      [ "constellation_64qam", "classgr_1_1wifi__pluto_1_1constellation__64qam.html", "classgr_1_1wifi__pluto_1_1constellation__64qam" ]
    ] ],
    [ "constellations_impl.h", "constellations__impl_8h.html", [
      [ "constellation_bpsk_impl", "classgr_1_1wifi__pluto_1_1constellation__bpsk__impl.html", "classgr_1_1wifi__pluto_1_1constellation__bpsk__impl" ],
      [ "constellation_qpsk_impl", "classgr_1_1wifi__pluto_1_1constellation__qpsk__impl.html", "classgr_1_1wifi__pluto_1_1constellation__qpsk__impl" ],
      [ "constellation_16qam_impl", "classgr_1_1wifi__pluto_1_1constellation__16qam__impl.html", "classgr_1_1wifi__pluto_1_1constellation__16qam__impl" ],
      [ "constellation_64qam_impl", "classgr_1_1wifi__pluto_1_1constellation__64qam__impl.html", "classgr_1_1wifi__pluto_1_1constellation__64qam__impl" ]
    ] ],
    [ "decode_mac.h", "decode__mac_8h.html", [
      [ "decode_mac", "classgr_1_1wifi__pluto_1_1decode__mac.html", "classgr_1_1wifi__pluto_1_1decode__mac" ]
    ] ],
    [ "frame_equalizer.h", "frame__equalizer_8h.html", "frame__equalizer_8h" ],
    [ "frame_equalizer_impl.h", "frame__equalizer__impl_8h.html", [
      [ "frame_equalizer_impl", "classgr_1_1wifi__pluto_1_1frame__equalizer__impl.html", "classgr_1_1wifi__pluto_1_1frame__equalizer__impl" ]
    ] ],
    [ "lms.h", "lms_8h.html", [
      [ "lms", "classgr_1_1wifi__pluto_1_1equalizer_1_1lms.html", "classgr_1_1wifi__pluto_1_1equalizer_1_1lms" ]
    ] ],
    [ "long_frame_synch.h", "long__frame__synch_8h.html", [
      [ "long_frame_synch", "classgr_1_1wifi__pluto_1_1long__frame__synch.html", "classgr_1_1wifi__pluto_1_1long__frame__synch" ]
    ] ],
    [ "ls.h", "ls_8h.html", [
      [ "ls", "classgr_1_1wifi__pluto_1_1equalizer_1_1ls.html", "classgr_1_1wifi__pluto_1_1equalizer_1_1ls" ]
    ] ],
    [ "mac.h", "mac_8h.html", [
      [ "mac", "classgr_1_1wifi__pluto_1_1mac.html", "classgr_1_1wifi__pluto_1_1mac" ]
    ] ],
    [ "mapper.h", "mapper_8h.html", "mapper_8h" ],
    [ "parse_mac.h", "parse__mac_8h.html", null ],
    [ "parse_mac_impl.h", "parse__mac__impl_8h.html", null ],
    [ "short_frame_sync.h", "short__frame__sync_8h.html", [
      [ "short_frame_sync", "classgr_1_1wifi__pluto_1_1short__frame__sync.html", "classgr_1_1wifi__pluto_1_1short__frame__sync" ]
    ] ],
    [ "signal_field.h", "signal__field_8h.html", [
      [ "signal_field", "classgr_1_1wifi__pluto_1_1signal__field.html", "classgr_1_1wifi__pluto_1_1signal__field" ]
    ] ],
    [ "signal_field_impl.h", "signal__field__impl_8h.html", [
      [ "signal_field_impl", "classgr_1_1wifi__pluto_1_1signal__field__impl.html", "classgr_1_1wifi__pluto_1_1signal__field__impl" ]
    ] ],
    [ "sta.h", "sta_8h.html", [
      [ "sta", "classgr_1_1wifi__pluto_1_1equalizer_1_1sta.html", "classgr_1_1wifi__pluto_1_1equalizer_1_1sta" ]
    ] ],
    [ "utils.h", "utils_8h.html", "utils_8h" ],
    [ "viterbi_decoder.h", "viterbi__decoder_8h.html", null ],
    [ "viterbi_decoder_generic.h", "viterbi__decoder__generic_8h.html", [
      [ "viterbi_decoder", "classgr_1_1wifi__pluto_1_1viterbi__decoder.html", "classgr_1_1wifi__pluto_1_1viterbi__decoder" ]
    ] ],
    [ "viterbi_decoder_x86.h", "viterbi__decoder__x86_8h.html", [
      [ "viterbi_decoder", "classgr_1_1wifi__pluto_1_1viterbi__decoder.html", "classgr_1_1wifi__pluto_1_1viterbi__decoder" ]
    ] ]
];