/* -*- c++ -*- */
/*
 * Copyright 2023 gr-wifi_pluto author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_WIFI_PLUTO_DECODE_MAC_H
#define INCLUDED_WIFI_PLUTO_DECODE_MAC_H

#include <wifi_pluto/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace wifi_pluto {

    /*!
     * \brief <+description of block+>
     * \ingroup wifi_pluto
     *
     */
    class WIFI_PLUTO_API decode_mac : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<decode_mac> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of wifi_pluto::decode_mac.
       *
       * To avoid accidental use of raw pointers, wifi_pluto::decode_mac's
       * constructor is in a private implementation
       * class. wifi_pluto::decode_mac::make is the public interface for
       * creating new instances.
       */
      static sptr make(bool log = false, bool debug = false);
    };

  } // namespace wifi_pluto
} // namespace gr

#endif /* INCLUDED_WIFI_PLUTO_DECODE_MAC_H */

