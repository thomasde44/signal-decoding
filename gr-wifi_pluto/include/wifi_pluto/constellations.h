/* -*- c++ -*- */
/*
 * Copyright 2023 gr-wifi_pluto author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_WIFI_PLUTO_CONSTELLATIONS_H
#define INCLUDED_WIFI_PLUTO_CONSTELLATIONS_H

#include <wifi_pluto/api.h>
#include <gnuradio/block.h>
#include <gnuradio/digital/constellation.h>
namespace gr {
  namespace wifi_pluto {

    /*!
     * \brief <+description of block+>
     * \ingroup wifi_pluto
     *
     */
    class WIFI_PLUTO_API constellation_bpsk : virtual public digital::constellation
    {
     public:
      typedef boost::shared_ptr<gr::wifi_pluto::constellation_bpsk> sptr;
      static sptr make();

     protected:
         constellation_bpsk();
    };

    class WIFI_PLUTO_API constellation_qpsk : virtual public digital::constellation
    {
     public:
      typedef boost::shared_ptr<gr::wifi_pluto::constellation_qpsk> sptr;
      static sptr make();

     protected:
         constellation_qpsk();
    };

    class WIFI_PLUTO_API constellation_16qam : virtual public digital::constellation
    {
     public:
      typedef boost::shared_ptr<gr::wifi_pluto::constellation_16qam> sptr;
      static sptr make();

     protected:
         constellation_16qam();
    };

    class WIFI_PLUTO_API constellation_64qam : virtual public digital::constellation
    {
     public:
      typedef boost::shared_ptr<gr::wifi_pluto::constellation_64qam> sptr;
      static sptr make();

     protected:
         constellation_64qam();
    };
  } // namespace wifi_pluto
} // namespace gr

#endif /* INCLUDED_WIFI_PLUTO_CONSTELLATIONS_H */

