/* -*- c++ -*- */
/*
 * Copyright 2023 gr-wifi_pluto author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_WIFI_PLUTO_LONG_FRAME_SYNCH_H
#define INCLUDED_WIFI_PLUTO_LONG_FRAME_SYNCH_H

#include <wifi_pluto/api.h>
#include <gnuradio/block.h>

namespace gr {
  namespace wifi_pluto {

    /*!
     * \brief <+description of block+>
     * \ingroup wifi_pluto
     *
     */
    class WIFI_PLUTO_API long_frame_synch : virtual public gr::block
    {
     public:
      typedef boost::shared_ptr<long_frame_synch> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of wifi_pluto::long_frame_synch.
       *
       * To avoid accidental use of raw pointers, wifi_pluto::long_frame_synch's
       * constructor is in a private implementation
       * class. wifi_pluto::long_frame_synch::make is the public interface for
       * creating new instances.
       */
      static sptr make(unsigned int sync_length, bool log, bool debug);
    };

  } // namespace wifi_pluto
} // namespace gr

#endif /* INCLUDED_WIFI_PLUTO_LONG_FRAME_SYNCH_H */

