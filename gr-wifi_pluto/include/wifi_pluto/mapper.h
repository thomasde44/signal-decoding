/* -*- c++ -*- */
/*
 * Copyright 2023 gr-wifi_pluto author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_WIFI_PLUTO_MAPPER_H
#define INCLUDED_WIFI_PLUTO_MAPPER_H

#include <wifi_pluto/api.h>
#include <gnuradio/block.h>


enum Encoding {
    BPSK_1_2 = 0,
    BPSK_3_4 = 1,
    QPSK_1_2 = 2,
    QPSK_3_4 = 3,
    QAM16_1_2 = 4,
    QAM16_3_4 = 5,
    QAM64_2_3 = 6,
    QAM64_3_4 = 7,
};

namespace gr {
  namespace wifi_pluto {

    /*!
     * \brief <+description of block+>
     * \ingroup wifi_pluto
     *
     */
    class WIFI_PLUTO_API mapper : virtual public gr::block
    {
    public:
        typedef boost::shared_ptr<mapper> sptr;
        static sptr make(Encoding mcs, bool debug = false);
        virtual void set_encoding(Encoding mcs) = 0;
    };

  } // namespace wifi_pluto
} // namespace gr

#endif /* INCLUDED_WIFI_PLUTO_MAPPER_H */

