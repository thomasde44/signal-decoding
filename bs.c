#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

int main(int argc, char **argv) {
    struct hci_dev_info di = { 0 };
    int dev_id, sock, len, flags, i, num_rsp;
    inquiry_info *info = NULL;
    char addr[19] = { 0 };
    
    // open socket and get local device info
    dev_id = hci_get_route(NULL);
    sock = hci_open_dev(dev_id);
    if (sock < 0) {
        perror("HCI device open failed");
        exit(1);
    }
    if (hci_devinfo(dev_id, &di) < 0) {
        perror("HCI device info failed");
        close(sock);
        exit(1);
    }
    
    // scan for nearby devices
    len = 8;  // inquiry length in seconds
    num_rsp = 10;  // maximum number of devices to find
    flags = IREQ_CACHE_FLUSH;  // inquiry flags
    info = (inquiry_info*)malloc(num_rsp * sizeof(inquiry_info));
    if (hci_inquiry(dev_id, len, num_rsp, NULL, &info, flags) < 0) {
        perror("HCI inquiry failed");
        close(sock);
        free(info);
        exit(1);
    }

    // print MAC addresses and names of found devices
    for (i = 0; i < num_rsp; i++) {
        ba2str(&(info+i)->bdaddr, addr);
        printf("Device %d: %s\n", i+1, addr);
        if ((info+i)->flags & IREQ_NAME) {
            printf("\tName: %s\n", (info+i)->name);
        }
    }

    // free memory and close socket
    free(info);
    close(sock);

    return 0;
}
